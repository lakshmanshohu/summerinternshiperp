/**
 * 
 */
package postgreSQLDatabase.forms;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import org.json.JSONArray;
import org.json.JSONObject;



/**
 * @author user
 *
 */
public class Query {

	/**
	 * @author Sattar
	 *
	 */
	public @interface java {

	}

 

	
	
	public static void addForm(String name,String fields,String format) throws SQLException{
	PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"addForm\"(?,?,?);");
	proc.setString(1, name);
	proc.setString(2, fields);
	proc.setString(3, format);
	
	proc.executeQuery();
}
	
	public static ArrayList<String> getFormNames(){
		
		ArrayList<String> list=new ArrayList<String>();
		
		try {
		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getFormName\"();");
			ResultSet rs=proc.executeQuery();
			while(rs.next()){
				list.add(rs.getString("form_name"));
				
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return list;
		
	}
	
	
	
	
public static String getFields(String formname){
		
		String fields ="SaM";
		
		try {
		PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT * from public.\"getRequiredFields\"(?);");
		proc.setString(1, formname);
	
			ResultSet rs=proc.executeQuery();
			 
			  
			
			  while(rs.next()){
			  
			  
			  
			 // System.out.println(rs);
			  
				fields=rs.getString("fields");
				//System.out.print(fields);
			  }
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return fields;
		
	}
	






public static String getFormat(String formname){
	
	String format ="SaM";
	
	try {
	PreparedStatement proc = settings.database.PostgreSQLConnection.getConnection().prepareStatement("SELECT public.\"getFormFormat\"(?);");
	proc.setString(1, formname);

		ResultSet rs=proc.executeQuery();
		
		  rs.next();
		  
		  
			format=rs.getString("format");
		
	} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
	}
	
	return format;
	
}
	
	
	
	
}
