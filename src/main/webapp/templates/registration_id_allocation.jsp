<%@page import="users.Student"%>
<%@page import="postgreSQLDatabase.registration.Query"%>
<html>
<head>


<style>

@page { size: A4;}
</style></head>
<body>
<title>REGISTRATION ID ALLOCATION</title>
	<p align="center"><img alt="" src="http://172.16.1.231/ftp/templates/IIITKLetterHead.png" style="height:99px; width:729px" /></p>

<%Student student=null;
if(request.getParameter("reg_id")!=null)
student=Query.getRegistrationStudentData(Long.parseLong(request.getParameter("reg_id"))); 
else if(request.getParameter("csab_id")!=null)
	student=Query.getCsabStudentProfile(Long.parseLong(request.getParameter("csab_id"))); 
else response.sendError(500);
%>

<p>Welcome to IIITK</p>
<p>Dear <%=student.getName() %></p>
<p>Your Registration ID is <%=student.getRegistration_id() %>,</p>

<h3 style="margin-left:7%;">Please follow following steps for registration process.</h5>

<ol type="1" style="margin-left:9%;">
<li>
Make sure you have submitted all Documents.</li>
<li>Login to ERP using above Registration ID.</li>
<li>Update your information,If you find non-editable prefield data incorrect contact the step 2 registration desk.</li>
<li>Update your changes and logout.</li>
<li>Go to registration desk,with all your documents and identity proofs for data verified</li>
<li>Once your data is verified go to thr PC and login again-Choose your payment method</li>
<li>Pay fees with your preffered medium.</li>
<li>If you choose challan save the challan copy as PDF or request the desk for a printout.</li>
<li>Logout and take the receipt to registration desk for verification</li>
<li>On Confirmation go back to PC and choose your ERP username and password and have your student ID Registered.</li>
</ol>
</body>
</html>
 



