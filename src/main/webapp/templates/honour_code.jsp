<html>
<head><center><h2>INDIAN INSTITUTE OF INFORMATION TECHNOLOGY,KOTA</h2></center></head>
<center><h2>THE HONOUR CODE</h2></center>
I ............................................................................................ID No..........................................................do hereby undertake that as a student at IIIT Kota<br>

<ol><li> I will not give or receive aid in examination; that I will give or receive unpermitted aid in class work, in preparation of reports, or in any other work that is to be used by the instructor as the basis of  grading ; and </li>
<li>I will do my share and take an active part in seeing to it that others as well as myself uphold the spirit and letter of the Honour Code.
I realize that some examples of misconduct which are regard as being in violation of the Honour Code include:</li></ol><br>
<ul>
<li>
Copying from another's examination paper or allowing another to copy from one's own paper;</li><br><li>
 Unpermitted  collaboration</li><br><li>
Plagiariasm ;</li><br><li>
Revising and resubmitting a marked quiz or examination paper for re-grading without the instructor's knowledge and consent;</li><br><li>
Giving and receiving unpermitted aid on take-home examinations;</li><br><li>
Representing as one's work the work of another  , including information available on internet and</li><br><li>
Giving or receiving and on an academic assignment under circumstances in which a reasonable person should have known that such aid was not permitted.</li><br><li>
Committing a cyber offense ,such as breaking passwords and accounts , sharing passwords , electronic copying, panting viruses, etc.</li><br><li>
I accept that any act of mine that can be considered to be an Honour Code violation will invite disciplinary action.<br>
   
       <table><tr> <td> <td>                      Student's signature............................................................
                         <tr>  <td> <td>               Name.................................................................................
<tr>  <td>      Date..................................................&nbsp;   &nbsp;   &nbsp;  &nbsp;   &nbsp;   &nbsp;   &nbsp; &nbsp;   &nbsp;   &nbsp;   &nbsp;       <td>ID No................................................................................</table></ul>
